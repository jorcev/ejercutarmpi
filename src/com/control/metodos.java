/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.control;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jorge Cevallos
 */
public class metodos {

    //Metodo para ejecutar el programa por medio de comandos
    public Process comandos(String comando) {
        Process resulComando = null;
        try {
            //vector con los parametros del comando a ejecutar
            String veccoman[] = {"bash", "-c", comando};
            resulComando = Runtime.getRuntime().exec(veccoman);
        } catch (IOException ex) {
            Logger.getLogger(metodos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resulComando;
    }

    public String leer(Process proceso) {
        String auxcad = "";
        String cad = "";
        try {
            //InputStreamReader sirve para leer caracteres pero los devuelve de forma suelta
            InputStreamReader ingrePath = new InputStreamReader(proceso.getInputStream());
            //BufferedReader al pedir una cadena completa de caracteres nos devulve un string
            BufferedReader leerarchiv = new BufferedReader(ingrePath);
            //el read line devuelve todos los caracteres tecleados hasta toparse con la tecla enter
            //y lo guarda en una variable de tipo string.
            cad = leerarchiv.readLine();
            do {
                auxcad += cad + "\n";
                cad = leerarchiv.readLine();
            } while (cad != null);
        } catch (IOException ex) {
            Logger.getLogger(metodos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return auxcad;
    }
}
